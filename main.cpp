#include <jni.h>
#include <stdio.h>

int main(int argc, char** argv) {

    JavaVM *vm;
    JNIEnv *env;
    JavaVMInitArgs vm_args;
    vm_args.version = JNI_VERSION_1_2;
    vm_args.nOptions = 0;
    vm_args.ignoreUnrecognized = 1;

    // Construction de la machine virtuelle Java (JVM).
    jint res = JNI_CreateJavaVM(&vm, (void **)&env, &vm_args);

    // Construct des données qui seront traitées par la fonction Java appelée.
    // Dans ce cas-ci, ce sera une valeur entière de "5".
    int valeur = 5;
    jvalue args[1];
    args[0].i=valeur;

    // Il faut trouver la classe sur laquelle on veut travailler. 
    jclass clazz = env->FindClass("UneFonction");

    // Trouver la méthode qu'on veut appeler dans la classe trouvée.
    // (I)I décrit la signature de la fonction, soit un unique "int" comme paramètre
    // et un "int" comme valeur de retour. Notez qu'il s'agit du type primitif
    // de java (int) et non du type objet (Integer).
    jmethodID cube = env->GetMethodID(clazz, "cube", "(I)I");

    // La fonction qu'on veut utiliser n'est pas statique. Il faut donc créer
    // l'objet auquel elle appartient. 
    // La méthode "<init>" fait référence au constructeur de la classe.
    // Sa signature est ()V, c'est-à-dire qu'elle ne prend aucun paramètre en
    // entrée et retourne "void", c'est-à-dire rien. Bref, c'est un constructeur
    // par défaut.
    jmethodID constructor = env->GetMethodID(clazz, "<init>", "()V");

    // On passe en Java !
    // On demande à Java de créer un objet de la classe UneFonction
    // en utilisant son constructeur par défaut.
    jobject une_fonction = env->NewObject(clazz, constructor, NULL);

    // On passe encore en Java !
    // On demande à Java d'exécuter la méthode "cube" de la classe
    // UneFonction avec les paramètres spécifiés par "args".
    // CallIntMethodA signifie appelle une méthode qui retourne le
    // type primitif de java "int" avec un nombre pré-déterminé
    // de paramètres.
    jint resultat = env->CallIntMethodA(une_fonction, cube, args);

    // Conversion du jint en int.
    int valeur_cubee = (int) resultat;

    // Affichage des résultats.
    printf("\nInput=%d, Cube=%d\n\n", valeur, valeur_cubee);

    // Fermeture de la machine virtuelle Java.
    vm->DestroyJavaVM();
}


