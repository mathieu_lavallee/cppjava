# Il est possible que vous ayiez à changer les valeurs des librairies Java si elles ne sont pas au même endroit. Voici les endroits de la machine virtuelle.

all:launch

cppjava: main.cpp UneFonction.class
	g++ -o cppjava -I /usr/lib/jvm/java-1.8.0-openjdk-1.8.0.191.b12-8.fc28.x86_64/include -I /usr/lib/jvm/java-1.8.0-openjdk-1.8.0.191.b12-8.fc28.x86_64/include/linux -L /usr/lib/jvm/java-1.8.0-openjdk-1.8.0.191.b12-8.fc28.x86_64/jre/lib/amd64/server/ -ljvm main.cpp

UneFonction.class:
	javac UneFonction.java

launch:cppjava
	LD_LIBRARY_PATH=/usr/lib/jvm/java-1.8.0-openjdk-1.8.0.191.b12-8.fc28.x86_64/jre/lib/amd64/server/ ./cppjava

clean:
	rm -rf cppjava
	rm -rf UneFonction.class
